---
layout: default
title: Install Guide
permlink: /installGuide/
sidebar: navibar
---


# uni10 installation instructions

Author: Adam Iaizzi - [iaizzi.me](https://www.iaizzi.me)

*Updated 2018-07-19*

Welcome to **uni10**! These instructions will guide you through the process of downloading, building and installing the library and then testing your installation by building and running an [example program](#example-program-using-uni10-my_itebd).
If you want a quick installation with most of the default options use the [quick install instructions](#quick-install). For a more detailed explanation of the process see the [detailed instructions](#detailed-instructions). If you get stuck consult the [FAQ](#faq).
**uni10** is still under active development and we are still working out all the kinks with the installation process. If you encounter trouble installing the library or building your code please contact us at the [**uni10** gitlab site](https://uni10.gitlab.io).

---

### Requirements

In many cases these prerequisites will already be installed on your system. There is more information on how to install them [here](#installing-prerequisites).

  * [cmake](http://cmake.org/) version > 2.8.12
  * C++ compiler with C++11 support
    * g++ >= 4.6.0
    * Intel C++ Compiler >= 15.0
    * Clang >= 9.0
    * Apple Clang >= 9.0
    * MSVC >=
  * BLAS and LAPACK libraries and header files
  * [Doxygen](http://www.stack.nl/~dimitri/doxygen/) (for documentation, optional)
  * For documentation **may also** need latex.  

---

### Quick Install

For a quick installation with mostly [default options](#build-options) follow this procedure and then [test your installation](#example-program-using-uni10-my_itebd)

    #Clone the repo (or download zip from gitlab)  
    git clone https://gitlab.com/uni10/uni10.git
    #Make a build directory  
    cd uni10
    mkdir build  
    cd build
    #Prepare for build
    cmake -DBUILD_DOC=on ..  
    #build
    make
    #install
    sudo make install

---

### Detailed instructions

*This instructions assume that you are using a unix-like system (macOS or linux) with access to the terminal.*

1. Install [all prerequisites](#installing-prerequisites) (such as [cmake](http://cmake.org/)) if they are not already installed.
2. First we will to download the repository from gitlab. Open the terminal and navigate to the directory where you want to download the repository. (This is **not** where the library will be installed).  
```
git clone https://gitlab.com/uni10/uni10.git
```  
3. Now prepare a build directory `uni10/build` (a temporary directory where you code **uni10** will be compiled before installation)  
```bash
cd uni10
mkdir build
cd build
```
4. Now we will configure the installation using `cmake` before doing the actual build. To install **uni10** with all default [options](#build-options) use the command
```
cmake ..
```  
This will set the installation path to `/usr/local/uni10`, if you you prefer to install **uni10** elsewhere (for example, in your home directory) you can **instead** use  
```
cmake -DCMAKE_INSTALL_PREFIX=~/lib/uni10/ ..
```
(Here you can replace `~/lib/uni10/` with a directory of your choosing).
If you want to build the documentation you can add the flag `-DBUILD_DOC=on` after `cmake`. For more information about build options see [this section](#build-options).     
5. Now we will compile the source by running  
```
make
```  
This may take a few minutes. It is normal to see a lot of compiler warnings as uni10 is built. At the end you should see `[100%] Built target runUni10Tests`. If you encounter errors in this step you may be missing some dependencies (like LAPACK or doxygen).
6. Now install the library into the proper directory you set in step **4**.
```
make install
```  
If you're installing to the default location you will need admin privileges, in which case you should **instead** use
```
sudo make install
```   
7. The **uni10** library is now installed on your machine. Before attempting to build your own code you can test the installation using the [example program](#example-program-using-uni10-my_itebd) below.

**Note:** If you encounter any errors in the build/installation process it is recommended to start delete the build directory and start again from **step 3**.

---

### Example program using uni10: my_iTEBD

You can test your installation of uni10 by building and running this simple example code ([my_iTEBD](http://physics.bu.edu/~iaizzi/files/my_iTEBD_ex.tar.gz)).
Navigate away form the build directory, then download and unzip the source files.
```bash
curl -O http://physics.bu.edu/~iaizzi/files/my_iTEBD_ex.tar.gz
tar -xzvf my_iTEBD_ex.tar.gz
cd my_iTEBD_ex
```
If you installed **uni10** somewhere other than the default directory (`/usr/local/uni10`), then you should modify the `Makefile` with the appropriate prefix:
```
INC := <YOUR_PREFIX>/include
LIB := <YOUR_PREFIX>/lib
```
Before compiling this binary, you must add the **uni10** library to the path using `export`. This command must be run every time before using uni10. You may want to add this line to your login script (e.g. `.bash_profile`) so it is run automatically.
```bash
#For Linux:
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/uni10/lib/
#For macOS:
export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/uni10/lib/  
```
If you used a custom installation directory in step **4**, replace `/usr/local/uni10` whatever custom prefix you used with the `cmake` command.

You can now compile `my_iTEBD.cpp` using the command  
```
make
```  
You may encounter compiler warnings during compilation, this is normal. You can now run `./a.out` and it should finish without errors.

**Note:** If you encounter errors during compilation or runtime related to the `-lgfortran` flag in `Makefile` try deleting that flag (this flag may or may not be necessary based on the version of BLAS and lapack you are using).

---

### Build Options

Option                       | Description (Default value)
----------------------------- | -------------------------------------------
BUILD_WITH_MKL               | Use Intel MKL for lapack and blas (off)
BUILD_WITH_INTEL_COMPILERS   | Use Intel C++ compiler  (off)
BUILD_EXAMPLES               | Build C++ examples (on)
BUILD_DOC                    | Build Documentation (off)
BUILD_CUDA_SUPPORT           | Build Library for CUDA GPUs (off)
CMAKE_INSTALL_PREFIX         | Installation location (/usr/local/uni10)

**Note:** The `cmake` flag for any of these options has a leading `-D`. For example to build with Intel MKL, use the flag `-DBUILD_WITH_MKL=on`

---

### Installing prerequisites
##### macOS/OSX

For macOS users, there are two possible sources for the prerequisites. The first is to use one of the package managers available for macOS, such as [homebrew](https://brew.sh/). Homebrew users will likely already know how to install all these prerequisites. For non-homebrew users,
1. Install macOS developer tools from Apple.
2. Install [cmake](http://cmake.org/) from the web.
3. *(optional)* install [Doxygen](http://www.stack.nl/~dimitri/doxygen/) for building the documentation.

##### Ubuntu

By default, Ubuntu does not come with all the prerequisites for building uni10. To install those prerequisites use the following commands before beginning the **uni10** build process (this will require admin privileges)
```bash
sudo apt-get install cmake
sudo apt-get install g++
sudo apt-get install gfortran
sudo apt-get install libblas-dev liblapack-dev
```
There are a few optional packages that will enable building documentation, etc.
```bash
sudo apt-get install git #for using git clone command
sudo apt-get install doxygen #for documentation
```

##### CentOS

Some versions of centos may not have all required dependencies to build **uni10**. To install these, use the command:
```bash
sudo yum install cmake gcc gcc-c++ blas blas-devel lapack lapack-devel
```  
If you want to build the documentation, you will also need  
```bash
sudo yum install doxygen
```  

---
### FAQ

**Q:** How to I **uninstall uni10**?  
**A:** Uninstalling uni10 is easy. Simply delete the library in `/usr/local/uni10` (or whatever prefix you used with the `cmake` command).

**Q:** Can I safely delete the build directory?  
**A:** Yes, once you have installed the library you no longer need the build directory of any of the files you downloaded. They can be safely deleted.

**Q:** Where is the documentation?  
**A:** The documentation is stored with the library in HTML format (provided you used the `cmake` option `-DBUILDDOC=on`). It can be accessed at [file:///usr/local/uni10/doc/html/index.html]

**Q:** Where should I install uni10?  
**A:** If you have admin privileges on the machine your are using, you may want to use the default install location `/usr/local/uni10`. If you do not have admin privileges, you may instead install uni10 in your home directory using the cmake command  
`cmake -DCMAKE_INSTALL_PREFIX=</installation_path>`

**Q:** I don't have admin privileges, can I install **uni10**?   
**A:** Yes! To install without admin privileges you can set the installation prefix to somewhere in your home directory like (for example) `~/lib/uni10`

**Q:** What if I do not have `git` installed on my system?  
**A:** If you don't have git you can download the [zip file from gitlab](https://gitlab.com/uni10/uni10/-/archive/master/uni10-master.zip).   
```
wget https://gitlab.com/uni10/uni10/-/archive/master/uni10-master.zip
```

**Q:** When running `cmake` I encounter an error:  
```
CMake Error at CMakeLists.txt:127 (project):
  No CMAKE_CXX_COMPILER could be found.

  Tell CMake where to find the compiler by setting either the environment
  variable "CXX" or the CMake cache entry CMAKE_CXX_COMPILER to the full path
  to the compiler, or to the compiler name if it is in the PATH.
```  
**A:** This error means your system does not have a c++ compiler installed. Install a c++ compiler. See [installing prerequisites](#installing-prerequisites).

**Q:** When running `cmake` I encounter an error:
```
CMake Error at /usr/share/cmake-3.10/Modules/FindBLAS.cmake:699 (message):
  A required library with BLAS API not found.  Please specify library
  location.
Call Stack (most recent call first):
  /usr/share/cmake-3.10/Modules/FindLAPACK.cmake:162 (find_package)
  CMakeLists.txt:176 (find_package)
  ```
**A:** This error means your system does not have the BLAS or LAPACK libraries installed. See [installing prerequisites](#installing-prerequisites).

**Q:** I installed cmake but the it still does not work from the command line (on macOS).  
**A:** You need to add the `cmake` binary to your path. Open the cmake graphical application, go to the menubar -> Tools -> How to Install For Command Line Use. This will open a window with a few different options for adding cmake to the command line. I recommend  
```
sudo "/Applications/CMake.app/Contents/bin/cmake-gui" --install
```

**Q:** How do I tell if `cmake` is installed on my system?  
**A:** Open a terminal and try the `cmake` command. If you receive an error to the effect of "command not found" you need to install cmake.

**Q:** How do I tell if Doxygen is installed on my system?  
**A:** Open a terminal and try the `doxygen` command. If you receive an error to the effect of "command not found" you need to install doxygen.

**Q:** How do I tell if BLAS and lapack installed on my system?  
**A:** There is no easy way to do check this other than attempting to follow the installation procedure. If you get an error related to BLAS or lapack you probably need to install those libraries.  


