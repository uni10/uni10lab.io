# Project Name


**Author:** Adam Iaizzi
**email:** iaizzi@bu.edu 
**website:** www.iaizzi.me

This program does a simple infinite Time-Evolved Bond Decimation (iTEBD) tensor network algorithm on the transverse field Ising Model (TFIM)
in imaginary time using time interval delta-T = 0.02 with 1,000 iterations and bond dimension  chi = 5. This program can be used to test a uni10 installation. See the uni10 [installation guide](http://uni10.gitlab.io/uni10.gitlab.io//InstallGuide.html)

Based on this [pyuni10 example code](http://uni10-tutorials.readthedocs.io/en/latest/_downloads/Tutorial1-2.ipynb) and [this accompanying tutorial](http://uni10-tutorials.readthedocs.io/en/latest/lecture1.html). 

## Quick Start 

Compile by executing `make` and then run using `./a.out`. You may need to change the location of the uni10 libraries and include folder in `Makefile` if you did not use the default values. 


## Prerequisites

The primary prerequisite is **uni10**. If that can be built and installed then this program should work fine. Tested with uni10 2.0.0.  


## Files

`my_iTEBD.cpp` --- Main program function.\ 
`Makefile` --- Makefile for building program.\
`results_example.txt` --- Example program output. 


## Inputs

None


## Outputs

This program write all results to standard out. To check if it ran correctly you can compare to the example results file `results_example.txt`. 


## References

[uni10 website](http://uni10.gitlab.io/) 
[uni10 installation guide](http://uni10.gitlab.io/uni10.gitlab.io//InstallGuide.html) 
[uni10 repository on Gitlab](https://gitlab.com/uni10/uni10)


## Known Issues


May generate warnings on compile. 
