/*
=======================================================
Created by Adam Iaizzi in 2018
iTEBD program for testing uni10 installation

Based on pyuni10 example code found at:
http://uni10-tutorials.readthedocs.io/en/latest/_downloads/Tutorial1-2.ipynb
and accompanying tutorial:
http://uni10-tutorials.readthedocs.io/en/latest/lecture1.html
=======================================================
field = 0
This code does iTEBD on the transverse field Ising Model (TFIM)
in imaginary time using time interval
 delta-T = 0.02
with 1000 iterations and bond dimension 
chi = 5

*/

#include "uni10.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <string>

using namespace std;
using namespace uni10;

//first run in bash:
//export DYLD_LIBRARY_PATH=/usr/local/uni10/lib/

//===========================================================
// FUNCTION DECLARATIONS
//===========================================================

//returns uni10 Matrix object for S+
Matrix<double> matSp();
//returns uni10 Matrix object for S-
Matrix<double> matSm();
//returns uni10 Matrix object for Sz
Matrix<double> matSz();
//make transverse Ising Hamiltonian
UniTensor<double> transverseIsing(double h);
//make Sx operator acting on left site
UniTensor<double> sxLeft();
//merge UniTensor T and Matrix L at bond idx
void bondcat(UniTensor<double> &T, Matrix<double> &L, int bidx);
//apply L^(-1) to bond idx of T
void bondrm(UniTensor<double> &T, Matrix<double> &L, int bidx);

//===========================================================
// MAIN
//===========================================================
int main(int argc, char* argv[])
{
  //pyuni10 example for iTEBD
  //http://uni10-tutorials.readthedocs.io/en/latest/lecture1.html
  cout << "=======================================================\n";
  cout << "Adam's iTEBD program\n";
  cout << "Based on pyuni10 example code found at:\n";
  cout << "http://uni10-tutorials.readthedocs.io/en/latest/_downloads/Tutorial1-2.ipynb\n";
  cout << "and accompanying tutorial:\n";
  cout << "http://uni10-tutorials.readthedocs.io/en/latest/lecture1.html\n";
  cout << "=======================================================\n";

  //basic parameters
  int chi = 5;    //bond dimension chi
  double delta = 0.02;  //time step delta
  int N = 1000;         //number of iterations
  double hh = 0.0;      // magnetic field
  // if a field has been supplied as an argument
  if (argc == 2)  hh = stod(argv[1])/1000.0;
  cout << "field = " << hh << endl;
    
  // tensor for two-site hamiltonian
  UniTensor<double> H = transverseIsing(hh);
  // tensor for Sx x iden operator
  UniTensor<double> sxop = sxLeft();
  
  cout << "This code does iTEBD on the transverse field Ising Model (TFIM)\n";
  cout << "in imaginary time using time interval\n delta-T = " << delta << endl;
  cout << "with " << N << " iterations and ";
  cout << "bond dimension \nchi = " << chi << endl;
  
  //initalize the bonds
  //chi in bonds
  Bond bdi_chi(BD_IN,chi);
  //chi out bonds
  Bond bdo_chi(BD_OUT,chi);
  //gamma matrices Gs=[Ga,Gb]
  vector<UniTensor<double>> Gs;
  //add Ga
  Gs.push_back(UniTensor<double>(vector<Bond> {bdi_chi,bdo_chi, H.bond(2)}, "Ga"));
  //add Gb
  Gs.push_back(UniTensor<double>(vector<Bond> {bdi_chi,bdo_chi, H.bond(2)}, "Gb"));
  //randomize Gs
  Gs[0].Randomize();
  Gs[1].Randomize();

  //set up lambda diagonal lambda matrices
  vector<Matrix<double>> Ls;
  Ls.push_back(Matrix<double>(chi,chi,true)); //diagonal matrix
  Ls.push_back(Matrix<double>(chi,chi,true)); //diagonal matrix
  //randomize Ls
  Ls[0].Randomize();
  Ls[1].Randomize();

  // set up imaginary time propagation operator U=e^[-delta H]
  UniTensor<double> U(H.bond(),"U");
  U.PutBlock(ExpH(-delta,H.GetBlock()));
  
  cout << sxop << endl;
  cout << U << endl;

  //--------------------------------
  cout << "\n\n========================================\n\n";
  cout << "\t\t Start doing updates \n";
  cout << "\n\n========================================\n\n";

  //main update loop, do N iterations
  //for (int i=0; i<200*N; i++){
  for (int i=0; i<N; i++){
    // for even i, do U^{AB} and for odd i do U^{BA}
    int A = i % 2;
    int B = (i+1)%2;
    
    //current state:
    //
    // ---Lb---Ga---La---Gb---Lb
    //          |         |
    //          |---------|
    //          |****U****|
    //          |---------|
    //          |         |
    //  Ga, Gb - rank 3 tensors for r and r+1
    //  La, Lb - diagonal matricies
    //  U - rank four tensor corresponding to the local imaginary time propagation operator
    //starting from this initial state, we want to contract to a single four leg tensor
    
    //first we multiply in the Lambda matrices
    //multiply La by appropriate index of Ga
    bondcat(Gs[A], Ls[A], 1);
    //multiply Lb by appropriate index of Ga
    bondcat(Gs[A], Ls[B], 0);
    //multiply by the appropriate index of 
    bondcat(Gs[B], Ls[B], 1);
    
    //set up labels for Ga, Gb and U for contraction
    Gs[A].SetLabel(vector<int> {  -1, 3,  1});
    Gs[B].SetLabel(vector<int> {  3,  -3, 2});
    U.SetLabel(vector<int> {  1,  2,  -2, -4});
    //contract Ga and Gb to make Ntheta 
    UniTensor<double> Ntheta = Contract(Gs[A],Gs[B]);
    // why do I save Ntheta? - for normalization later
    
    // permute labels of sx operator act on Ntheta
    sxop.SetLabel(vector<int> {  1,  2,  -2, -4});
    // contract sxop with Ntheta
    UniTensor<double> sxdone = Contract(Ntheta,sxop);
    //permute result so sxdone is ready to contract with Ntheta
    sxdone = Permute(sxdone,vector<int> {-1, -2, -3, -4}, 2);

    // make final theta by contracting preliminary theta with U
    UniTensor<double> theta = Contract(Ntheta,U);
    //permute result for SVD so indices are [alpha i]:[j gamma]
    theta = Permute(theta,vector<int> {-1, -2, -3, -4}, 2);

    //do singular value value decomposition to pull out X, and Y (tensors) and new La
    vector<Matrix<double>> svd = Svd(theta.GetBlock());
    //output: three matrices
    //svd[0] = u  unitary matrix - becomes X in 1 May 2018 notes
    //svd[1] = st diagonal matrix - becomes lambda-tilde A
    //svd[2] = vt unitary matrix - becomes Y

    //****************
    //before we can set up the update Ga and Gb we need to truncate (decimate)
    //****************
    //truncations
    Matrix<double> sv = svd[1];
    //resize sv, taking only first chi x chi elements in matrix
    Resize(sv,chi,chi,INPLACE);
    //take the norm of sv
    double norm = Norm(sv);
    //normalize sv
    sv = sv * (1.0/norm);
    
    ///?????????? What do these weights mean?

    //sv becomes the new Ls[A]
    Ls[A] = sv;
    
    //truncate svd[0] to be a ???? x chi matrix for Ga
    Resize(svd[0],svd[0].row(),chi,INPLACE);

    // different than python example: had to add a permute to make dimensions match
    Gs[A] = Permute(Gs[A],vector<int> {-1,1,3},2);
    // u becomes new Ga
    Gs[A].PutBlock(svd[0]);
    // truncate vt
    Resize(svd[2],chi,svd[2].col(),INPLACE);
    // vt becomes new Gb
    Gs[B].PutBlock(svd[2]);
    Gs[A] = Permute(Gs[A],vector<int> {-1,3,1},1);
    
    // now we have this diagram (step iii from 2018 May 1 notes)
    //
    // ---X---La'---Y---
    //    |         |
    //    |         |
    // 
    //  X, Y - rank 3 tensors for r and r+1
    //  La - diagonal matrix 
    
    //multiply Ga and Gb by inverse of Lb to recover original diagram
    bondrm(Gs[A],Ls[B],0);
    bondrm(Gs[B],Ls[B],1);
    
    //step iv
    //
    // ---Lb---(Lb)^-1---X---La'---Y---(Lb)^-1---Lb
    //                   |         |
    //                   |         |
    //
    // step v 
    //
    // ---Lb---Ga'---La'---Gb'---Lb
    //          |          |
    //          |          |
    //          |          |
    //
    // now we have finished this update
    
    // -----------------------------------------------------------------
    // now just extract observables
    
    //contract theta with itself to get a scalar
    double val = (Contract(theta,theta))[0];
    
    //cout << Contract(theta,theta);
    // ^^ if this line is not present, the precision of cout is set to integer
    // ^^ somehow a previous function is affecting the default precision of cout (which should not occur)
    
    //norm
    norm = (Contract(Ntheta,Ntheta))[0];
    //cout << "E= norm=" << norm << endl;
    double E = -log(val) / delta / 2.0;
    //????? where does this factor of 2 come from? - because theta already has U, so contracting theta, theta = <2U>
    
    //sx operator
    double sxval = Contract(sxdone,Ntheta)[0];
    double sx2   = Contract(sxdone,sxdone)[0];

    //normalize
    E       = E/norm;
    sxval   = sxval/norm;
    sx2     = sx2/norm;
    
    //cout << "val " << E << endl;
    //if ( i%100 == 0)
    //  cout << "E=\t" << E << "\tS_x=\t" << sxval << endl;

    //at end print values to screen
    if ((i+1)%N == 0){
        cout << hh << "\t" << E << "\t" << sxval << "\t" << sx2 << endl;
        //hh+=0.05;
        //H = transverseIsing(hh);
        //U.PutBlock(ExpH(-delta,H.GetBlock()));
    }
        

  }

  /*
  //try permuting
  UniTensor<double> test( vector<Bond> {Bond(BD_IN,2),Bond(BD_IN,2),Bond(BD_OUT,2)});
  double elem[8] = {1,2,3,4,5,6,7,8};
  test.SetElem(elem);
  test.SetLabel(std::vector<int> {1,2,3});
  cout<<test;
  test=Permute(test,std::vector<int> {3,1,2},1);
  cout<<test;
  */
  return(0);
}

//===========================================================
// FUNCTION IMPLEMENTATIONS
//===========================================================
Matrix<double> matSp()
{
  double spin = 0.5;
  int dim = spin*2+1;
  //create non-diagonal matrix for return
  Matrix<double> mat(dim,dim,false);
  //set elements of mat
  mat.SetElem(vector<double> {0.0, 1.0, 0.0, 0.0});
  return(mat);
}

Matrix<double> matSm()
{
  double spin = 0.5;
  int dim = spin*2+1;
  //create non-diagonal matrix for return
  Matrix<double> mat(dim,dim,false);
  //set elements of mat
  mat.SetElem(vector<double> {0.0, 0.0, 1.0, 0.0});
  return(mat);
}

Matrix<double> matSz()
{
  double spin = 0.5;
  int dim = spin*2+1;
  //create non-diagonal matrix for return
  Matrix<double> mat(dim,dim,true);
  //set elements of mat
  mat.SetElem(vector<double> {0.5, -0.5});
  return(mat);
}

//make transverse Ising Hamiltonian
UniTensor<double> sxLeft()
{
  double spin =0.5;
  int dim = 2*spin+1;
  //define S_x operator
  Matrix<double> sx = 0.5*(matSp()+matSm());
  //define identity operator
  Matrix<double> iden(dim,dim,true); //what if i set this to false?
  iden.Identity();

  //two-site operator tensor formed of outer products of spin operators on site i and identity on site j
  Matrix<double> sxmat = Otimes(2*sx,iden);

  //build bonds
  Bond bdi(BD_IN,dim);    // in
  Bond bdo(BD_OUT,dim);   // out
  // create new tensor using a vector of the bonds and name it SxTens
  UniTensor<double> sxten(vector<Bond> {bdi, bdi, bdo, bdo},"SxTens");

  //add elements to H
  sxten.PutBlock(sxmat);

  return(sxten);
}

//make transverse Ising Hamiltonian
UniTensor<double> transverseIsing(double h)
{
  double spin =0.5;
  int dim = 2*spin+1;
  //define S_x operator
  Matrix<double> sx = 0.5*(matSp()+matSm());
  //define S_z operator
  Matrix<double> sz = matSz();
  //define identity operator
  Matrix<double> iden(dim,dim,true); //what if i set this to false?
  iden.Identity();

  //two-site hamiltonian tensor formed of outer products of spin operators on site i and j
  Matrix<double> ham = Otimes(2*sz,2*sz) + 0.5*h*(Otimes(iden,2*sx)+Otimes(2*sx,iden));

  //build bonds
  Bond bdi(BD_IN,dim);    // in
  Bond bdo(BD_OUT,dim);   // out
  // create new tensor using a vector of the bonds and name it TFIM
  UniTensor<double> H(vector<Bond> {bdi, bdi, bdo, bdo},"TFIM");

  //add elements to H
  H.PutBlock(ham);

  return(H);
}

//merge UniTensor T and matrix L at bond idx
void bondcat(UniTensor<double> &T, Matrix<double> &L, int bidx)
{
  //takes a tensor T and a matrix L
  //get labels from T
  vector<int> labels = T.label();
  //copy over to per_labels
  vector<int> per_labels = labels;
  //create an order to permute the labels
  //insert move label at position bidx to beginning of per_labels
  per_labels.insert(per_labels.begin(),per_labels.at(bidx));
  per_labels.erase(per_labels.begin()+bidx+1);
  
  // how many inward bonds are there for T?
  int inBondNum = T.InBondNum();
  //permute T so idx is the first index and the only inbound one
  T = Permute(T,per_labels,1);
  //multiply diagonal L matrix by properly arranged block of T
  T.PutBlock( Dot(L,T.GetBlock()) );
  //Permute T back to its original form
  T = Permute(T,labels,inBondNum);
}

//apply L^(-1) to bond idx of T
void bondrm(UniTensor<double> &T, Matrix<double> &L, int bidx)
{
  //create a matrix to hold the inverted version of L
  Matrix<double> invL(L.row(),L.col(),true);
  //loop over all elements of L and invert
  for (int i = 0; i < L.ElemNum(); i++) {
    if (L[i] ==0)
      invL[i] = 0;
    else
      invL[i] = 1/L[i]; //this works because it is diagonal
  }
  bondcat(T,invL,bidx);
}
